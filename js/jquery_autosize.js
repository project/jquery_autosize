/**
 * Javascript for applying JQuery Autosize to textareas.
 */

(function ($, Drupal) {
  Drupal.behaviors.navTabs = {
    attach: function attach(context) {
      $('textarea', context)
        .not('.js-autosize-processed')
        .autosize()
        .addClass('js-autosize-processed');
    }
  };
})(jQuery, Drupal);
