# jQuery autosize

A plugin to automatically adjust textarea height.

* [Library](https://www.jacklmoore.com/autosize/)
* [Github](https://github.com/jackmoore/autosize)
* Alternative module [expanding textareas](https://www.drupal.org/project/expanding_textareas)

## Installation

### Composer (recommended)

```
composer require drupal/jquery_autosize
```

## Configuration

Minimal configuration options. Once enabled, all textareas on your site will 
adopt the autosize functionality.
 * Module configuration options at `admin/config/user-interface/jquery_autosize`.
